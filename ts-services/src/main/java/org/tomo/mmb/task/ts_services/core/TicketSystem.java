package org.tomo.mmb.task.ts_services.core;

import java.util.LinkedList;
import java.util.List;
import org.tomo.mmb.task.ts_services.exception.MyBusinessException;
import org.tomo.mmb.task.ts_services.util.TicketIdSequence;

/**
 * <h1>Ticket System.</h1>
 *
 * Thread-safe Singleton.
 * <p>
 * Service class implementing The Ticket System API operations from
 * {@link ITicketSystem}.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class TicketSystem implements ITicketSystem {

    /**
     * Singleton instance.
     */
    private static TicketSystem instance = null;

    /**
     * The Ticket System Queue.
     *
     * Queue with active tickets.
     *
     * @see ITicket
     */
    private List<ITicket> queue = null;

    /**
     * Instance of {@link org.tomo.mmb.task.ts_services.util.TicketIdSequence}.
     *
     * Ticket id auto-incremental generator.
     */
    private TicketIdSequence ticketIdSequence = null;

    /**
     * <h1>Constructor.</h1>
     *
     * <ul>
     * <li>Initialize new, empty queue for tickets.</li>
     * </ul>
     *
     * @param ticketIdSequence Instance of
     * {@link org.tomo.mmb.task.ts_services.util.TicketIdSequence}. Must not be
     * null.
     */
    private TicketSystem(final TicketIdSequence ticketIdSequence) {

        this.queue = new LinkedList<>();

        this.ticketIdSequence = ticketIdSequence;
    }


    @Override
    public INewTicketResult createNewActiveTicket() throws MyBusinessException {

        // first, generate next non-used ticket id        
        Ticket new_ticket = new Ticket(this.ticketIdSequence.getNextId());

        int new_ticket_order = -99;

        synchronized (this) {

            this.queue.add(new_ticket);

            new_ticket_order = this.queue.size() - 1;
        }

        INewTicketResult result = new NewTicketResult(new_ticket, new_ticket_order);

        return result;
    }


    /**
     *
     * @return int
     * @throws MyBusinessException id: 4, when ticket queue is empty
     */
    @Override
    public int getFirstActiveTicketNumber() throws MyBusinessException {

        if (this.queue.isEmpty()) {
            throw new MyBusinessException(4, "ticket queue is empty");
        }

        ITicket first_ticket = this.queue.get(0);

        int first_ticket_id = first_ticket.getId();

        return first_ticket_id;
    }


    /**
     *
     * @throws MyBusinessException id: 5, when ticket queue is empty
     */
    @Override
    public synchronized void destroyLastActiveTicket() throws MyBusinessException {

        if (this.queue.isEmpty()) {
            throw new MyBusinessException(5, "ticket queue is empty");
        }

        int queue_last_index = this.queue.size() - 1;

        this.queue.remove(queue_last_index);
    }


    @Override
    public List<ITicket> getAllActiveTickets() {

        List<ITicket> queue_copy = new LinkedList(queue);

        return queue_copy;
    }


    /**
     * <h1>Returns instance of this singleton class.</h1>
     *
     * If called first, new instance with
     * {@link org.tomo.mmb.task.ts_services.util.TicketIdSequence} object is
     * created in thread-safe manner.
     *
     * @return {@link TicketSystem}
     */
    public static TicketSystem getInstace() {

        if (TicketSystem.instance == null) {

            synchronized (TicketSystem.class) {

                TicketIdSequence ticket_id_sequence_instance = TicketIdSequence.getInstance();

                TicketSystem.instance = new TicketSystem(ticket_id_sequence_instance);
            }
        }

        return TicketSystem.instance;
    }


    /**
     * Only for test purposes.
     */
    static void test_removeInstanceReference() {
        TicketSystem.instance = null;
    }


}
