package org.tomo.mmb.task.ts_services.core;

/**
 * <h1>Result of creating new ticket operation.</h1>
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 * @see INewTicketResult
 */
public class NewTicketResult implements INewTicketResult {

    /**
     * New ticket entity instance.
     *
     * @see ITicket
     */
    private ITicket ticket = null;

    /**
     * Order in queue of new created ticket entity instance.
     */
    private int queueOrder = -99;

    /**
     * @param ticketEntity new created ticket entity
     * @param queueOrder order in queue of new created ticket
     */
    public NewTicketResult(final ITicket ticketEntity, final int queueOrder) {

        this.ticket = ticketEntity;
        this.queueOrder = queueOrder;
    }


    @Override
    public ITicket getTicket() {

        return this.ticket;
    }


    @Override
    public int getQueueOrder() {

        return this.queueOrder;
    }


    /**
     * <h1>String representation of result of new ticket creation
     * operation.</h1>
     *
     * String format example:
     * <p>
     * {@code no. #23  order: 6 Fri Sep 22 02:00:00 EET 2017}
     *
     * @return {@link java.lang.String}
     */
    @Override
    public String toString() {

        return "no. #" + Integer.toString(this.getTicket().getId()) + "\torder: " + Integer.toString(this.getQueueOrder()) + "\t" + this.getTicket().getDate().toString();
    }


}
