package org.tomo.mmb.task.ts_services.core;

import java.util.Date;

/**
 * <h1>Ticket entity.</h1>
 *
 * Describe one ticket in ticket queue.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public interface ITicket {

    /**
     * <h1>Ticket id (or ticket number).</h1>
     *
     * @return int - ticket id (or ticket number)
     */
    public int getId();


    /**
     * <h1>Creation date and time.</h1>
     *
     * Return date and time, when ticket object was created.
     *
     * @return {@link java.util.Date} - ticket creation date and time
     */
    public Date getDate();


    /**
     * <h1>String representation of ticket.</h1>
     *
     * @return {@link java.lang.String}
     */
    @Override
    public String toString();


}
