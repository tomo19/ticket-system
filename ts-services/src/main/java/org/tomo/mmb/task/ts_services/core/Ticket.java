package org.tomo.mmb.task.ts_services.core;

import java.util.Date;

/**
 * <h1>Ticket entity.</h1>
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class Ticket implements ITicket {

    /**
     * Unique ticket id (or ticket number).
     */
    private int id = -99;

    /**
     * Date of creation new ticket entity instance.
     */
    private Date date = null;

    /**
     * <h1>Constructor.</h1>
     *
     * {@code id} must be an unique ticket number of new created ticket. For
     * now, there is no checking if ticket id is really unique.
     *
     * @param id unique ticket id
     */
    public Ticket(final int id) {

        this.id = id;
        this.date = new Date();
    }


    @Override
    public int getId() {

        return this.id;
    }


    @Override
    public Date getDate() {

        return this.date;
    }


}
