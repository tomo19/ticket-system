package org.tomo.mmb.task.ts_services.util;

import org.tomo.mmb.task.ts_services.exception.MyBusinessException;

/**
 * <h1>Auto-increment number sequence.</h1>
 *
 * <ul><li>Singleton.</li></ul>
 * <p>
 * Used for generating unique ticket id (number).
 * <p>
 * Counter will increment +1 after each {@link #getNextId() } call.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class TicketIdSequence {

    /**
     * Singleton instance of this class.
     */
    private static TicketIdSequence instance = null;

    /**
     * Ticket id (number) counter variable. Last <b>used</b> id.
     */
    private int lastId = 0;

    /**
     * <h1>Constructor.</h1>
     *
     * Initialize counter to last used value = 0. So, first generated ticket id
     * will be 1.
     */
    private TicketIdSequence() {
        this.lastId = 0;
    }


    /**
     * <h1>Generate next ticket id (number).</h1>
     *
     * Counter will increment +1 after each {@link #getNextId() } call.
     * <p>
     * Thread-safe method.
     *
     * @return int - next free ticket id (number)
     * @throws MyBusinessException id: 3, when all possible integers in range of
     * &lt;1, {@link java.lang.Integer#MAX_VALUE}&gt; was generated.
     */
    public synchronized int getNextId() throws MyBusinessException {

        if (this.lastId == Integer.MAX_VALUE) {
            throw new MyBusinessException(3, "there is no one free ticket");
        }

        this.lastId += 1;

        return this.lastId;
    }


    /**
     * <h1>Return singleton instance of this class.</h1>
     *
     * First call will create a new singleton instance of this class.
     *
     * @return {@link TicketIdSequence}
     */
    public static TicketIdSequence getInstance() {

        if (TicketIdSequence.instance == null) {

            synchronized (TicketIdSequence.class) {
                TicketIdSequence.instance = new TicketIdSequence();
            }
        }

        return TicketIdSequence.instance;
    }


    /**
     * Only for test purposes.
     */
    public static void test_removeInstanceReference() {
        TicketIdSequence.instance = null;
    }


}
