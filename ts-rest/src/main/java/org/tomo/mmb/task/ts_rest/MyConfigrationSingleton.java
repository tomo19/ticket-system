package org.tomo.mmb.task.ts_rest;

import org.tomo.mmb.task.ts_services.core.ITicketSystem;
import org.tomo.mmb.task.ts_services.core.TicketSystem;

/**
 * My configuration bean.
 *
 * <ul>
 * <li>Singleton</li>
 * <li>Application scope</li>
 * <li>POJO</li>
 * </ul>
 *
 * <p>
 *
 * Contains references to another modules and APIs - The Ticket System.
 * <p>
 * This class is layer between JAX-RS API and
 * {@link org.tomo.mmb.task.ts_services} core Ticket System services.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class MyConfigrationSingleton {

    /**
     * Singleton instance of this class.
     */
    private static MyConfigrationSingleton instance = null;

    /**
     * Reference to The Ticket System core services instance.
     */
    private ITicketSystem ticketSystem = null;

    private MyConfigrationSingleton(final ITicketSystem ticketSystem) {

        this.ticketSystem = ticketSystem;
    }


    public static MyConfigrationSingleton getInstance() {

        if (MyConfigrationSingleton.instance == null) {

            synchronized (MyConfigrationSingleton.class) {

                // first, create instance of Ticket System (API)
                //
                MyConfigrationSingleton.instance = new MyConfigrationSingleton(TicketSystem.getInstace());
            }
        }

        return MyConfigrationSingleton.instance;
    }


    /**
     * Returns reference to The Ticket System core services instance.
     *
     * @return {@link org.tomo.mmb.task.ts_services.core.ITicketSystem}
     */
    public ITicketSystem getITicketSystem() {

        return this.ticketSystem;
    }


}
