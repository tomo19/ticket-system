package org.tomo.mmb.task.ts_rest.rest_resources;

import java.net.URI;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.tomo.mmb.task.ts_rest.MyConfigrationSingleton;
import org.tomo.mmb.task.ts_rest.messages.ErrorXMLEntity;
import org.tomo.mmb.task.ts_rest.messages.ObjectFactory;
import org.tomo.mmb.task.ts_rest.messages.TicketXMLEntity;
import org.tomo.mmb.task.ts_services.core.INewTicketResult;
import org.tomo.mmb.task.ts_services.exception.MyBusinessException;
import org.tomo.mmb.task.ts_services.util.DateConversion;

/**
 * <h1><b>Ticket</b> REST resource.</h1>
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
@Path("ticket")
public class TicketREST {

    private static final Logger logger = Logger.getLogger("TicketREST");

    @Context
    UriInfo context;

    /**
     * Creates a new instance of Ticket
     */
    public TicketREST() {
    }


    /**
     * GET method for getting ticket
     *
     * @return {@link javax.ws.rs.core.Response}
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Response show() {

        // RESPONSE ! ! !
        //
        return Response.status(Response.Status.FORBIDDEN)
                .entity("")
                .build();
    }


    /**
     * POST method for creating an instance of Ticket
     *
     * @return {@link javax.ws.rs.core.Response}
     */
    @POST
    @Produces(MediaType.APPLICATION_XML)
    public Response create() {

        // create new ticket in Ticket System
        // ---------------------------------------------------------------------
        //
        INewTicketResult new_ticket_result = null;

        try {
            new_ticket_result = MyConfigrationSingleton.getInstance().getITicketSystem().createNewActiveTicket();

            logger.log(Level.INFO, "TicketREST.create(): created new ticket: " + new_ticket_result.toString());

        } catch (MyBusinessException e) {

            logger.log(Level.SEVERE, "TicketREST.create(): error during creating new ticket");
            logger.log(Level.SEVERE, "TicketREST.create(): Exception msg: ");
            logger.log(Level.SEVERE, e.getMessage());

            // create Error REST Response with error message
            // -----------------------------------------------------------------
            //
            ErrorXMLEntity error_entity = new ErrorXMLEntity();

            error_entity.setId(e.getMyExceptionId());
            error_entity.setMessage(e.getMessage());

            JAXBElement<ErrorXMLEntity> error_entity_JAXB = new ObjectFactory().createError(error_entity);

            // RESPONSE ! ! !
            //
            return Response.status(Response.Status.BAD_REQUEST)
                    .encoding("UTF-8")
                    .entity(error_entity_JAXB)
                    .build();
        }

        // prepare data for REST response
        // ---------------------------------------------------------------------
        //
        int new_ticket_id = new_ticket_result.getTicket().getId();

        Date new_ticket_created_at_java_date = new_ticket_result.getTicket().getDate();

        // date as XMLGregorianCalender for XML date/time fields in JAXB
        XMLGregorianCalendar new_ticket_created_at_XML_date = null;

        try {
            new_ticket_created_at_XML_date = DateConversion
                    .getInstace().convertDateForXML(new_ticket_created_at_java_date);
        } catch (DatatypeConfigurationException e) {

            // RESPONSE ! ! !
            //
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .encoding("UTF-8")
                    .entity("")
                    .build();
        }

        int new_ticket_queue_order = new_ticket_result.getQueueOrder();

        // create POJO XML entity for conversion to XML (JAXB)
        // based on: org.tomo.mmb.tas.ts_rest.messages.xsd.TicketXMLEntity.xsd
        // ---------------------------------------------------------------------
        //
        TicketXMLEntity ticket_XML_entity = new TicketXMLEntity();

        ticket_XML_entity.setId(new_ticket_id);
        ticket_XML_entity.setCreatedAtDate(new_ticket_created_at_XML_date);
        ticket_XML_entity.setCreatedAtTime(new_ticket_created_at_XML_date);
        ticket_XML_entity.setQueuePosition(new_ticket_queue_order);

        JAXBElement<TicketXMLEntity> ticket_JAXB = new ObjectFactory().createTicket(ticket_XML_entity);

        // create REST response
        // ---------------------------------------------------------------------
        //
        URI created_resource_URI = UriBuilder.fromUri(this.context.getRequestUri())
                .path(Integer.toString(new_ticket_id))
                .build();

        logger.log(Level.INFO, "TicketREST.create(): returned URL: " + created_resource_URI.toString());

        // RESPONSE ! ! !
        //        
        return Response.created(created_resource_URI)
                .encoding("UTF-8")
                .entity(ticket_JAXB)
                .build();
    }


}
