package org.tomo.mmb.task.ts_rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Java EE REST (JAX-RS) Application entry point and configuration.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
@ApplicationPath(ApiDescription.ROOT_PATH_REST)
public class ApplicationConfig extends Application {

    /**
     * Initialization of JAX-RS application.
     */
    public ApplicationConfig() {

        // initialization, first call is longer operation, creation of singleton instance,
        // after that, second call is only instance getter
        //
        MyConfigrationSingleton.getInstance();
    }


}
