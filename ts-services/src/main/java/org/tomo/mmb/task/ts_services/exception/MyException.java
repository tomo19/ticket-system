package org.tomo.mmb.task.ts_services.exception;

/**
 * <h1>My own exception with exception id.</h1>
 *
 * Exception id is helpful for finding exact place of exception from logs.
 * Because of this, exception id should be unique.
 *
 * Example of exception message format:
 * <p>
 * {@code Ex. id: 23: there is no such an active ticket in ticket queue}
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class MyException extends Exception {

    private int exceptionId = -1;

    private String message = "";

    /**
     * Constructs an instance of <code>MyException</code> with the specified
     * detail message.
     *
     * @param exceptionId the id of exception.
     * @param message the detail message.
     */
    public MyException(int exceptionId, String message) {

        super("Ex. id: #" + Integer.toString(exceptionId) + ": " + message);

        this.exceptionId = exceptionId;

        this.message = "Ex. id: #" + Integer.toString(exceptionId) + ": " + message;
    }


    public int getMyExceptionId() {
        return this.exceptionId;
    }


}
