package org.tomo.mmb.task.ts_services.core;

import java.util.List;
import org.tomo.mmb.task.ts_services.exception.MyBusinessException;

/**
 * <h1>Ticket System Facade.</h1>
 *
 * Interface act as Tiket System API.
 * <p>
 * For now, you must create new instance of Ticket System with
 * {@link TicketSystem} class, which implements this interface.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 * @see TicketSystem
 */
public interface ITicketSystem {

    /**
     * <h1>Create new ticket.</h1>
     *
     * Create new ticket and put it at the end of ticket queue. New ticket must
     * has a unique ticket id.
     * <p>
     * You can find new ticket order in queue in returned
     * {@link INewTicketResult} object.
     *
     * @return {@link INewTicketResult} - new ticket entity and other
     * informations
     * @throws MyBusinessException exceptions from
     * {@link org.tomo.mmb.task.ts_services.util.TicketIdSequence}
     * @see org.tomo.mmb.task.ts_services.util.TicketIdSequence
     */
    public INewTicketResult createNewActiveTicket() throws MyBusinessException;


    /**
     * <h1>First active ticket id in ticket queue.</h1>
     *
     * @return int
     * @throws MyBusinessException when ticket queue is empty
     */
    public int getFirstActiveTicketNumber() throws MyBusinessException;


    /**
     * <h1>Remove last active ticket in ticket queue.</h1>
     *
     * @throws MyBusinessException when ticket queue is empty
     */
    public void destroyLastActiveTicket() throws MyBusinessException;


    /**
     * <h1>List of all active tickets in ticket queue.</h1>
     *
     * Returns list of all active tickets in ticket queue.
     * <p>
     * <b>Note:</b> Must return copy of this list, not references to tickets in
     * ticket queue.
     *
     * @return {@code List<ITicket>}
     * @see java.util.List
     * @see ITicket
     */
    List<ITicket> getAllActiveTickets();


}
