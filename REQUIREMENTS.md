# Minimal Software Requirements #

## Build and runtime requirements #

The Ticket System was developed with these versions of software:

| Software | Version |
| :---: | :---: |
| JDK | 1.8.0.131 |
| JRE | 1.8.0.131 |
| Apache Maven | 3.3.3 |

Developed on Java EE Server:

| Software | Version |
| :---: | :---: |
| GlassFish Server Open Source Edition | 4.1 |

### Java EE Server ###

You must use **Java EE 7 compatible application server**. For running The Ticket System, you need **only web container and its APIs**. You don't need EJB container.

It was not tested on only partial (not fully compatible) implementations of Java EE 7 web containers, like Jetty, or Tomcat. 

>This build is not bundled with third-party implementations of Java EE APIs, it uses Java EE APIs (for web container) provided on Java EE application servers. Because of this, it will not run properly if your server platform implements only partial Java EE web container APIs, or if it is dedicated for running only servlet pure modules.


## Other ##

Other right versions of software development frameworks and libraries used in implementation you will find in all Maven project `pom.xml` files inside all Maven modules (including parent Maven module).