# README #

MMB programming task: The Ticket System

* web page
  <http://tomo19.alwaysdata.net/hr_recruitment/en/mmb-task>


## 1. Installation and deployment ##

1. build Java WAR module
2. deploy it on compatible Java EE application server

You must have installed Java JRE + Java JDK on your system, and Maven build for building Java WAR module.

### 1.1. Build ###
     
```sh
cd ticket-system
mvn clean install
```

* builded WAR package:

```sh
ticket-system/ts-rest/target/ts-rest-[VERSION].war
```

### 1.2. Deploy ###

Manually deploy the built WAR module to your Java EE Server. Copy it to deployment directory on the server, or use administration interface of your server. See details how to deploy in your documentation for your concrete Java EE Server.

---


## 2. Modules ##

### 2.1. Summary ###

| GroupId | ArtifactId | package | Java package |
| :---: | :---: | :---: | :--- |
| **org.tomo.mmb.task.ticket-system** | **ticket-system** | POM | |
| | **ts-rest** | WAR | `org.tomo.mmb.task.ts_rest` |
| | **ts-services** | JAR | `org.tomo.mmb.task.ts_services` |

### 2.2. ts-rest ###

* REST web interface (web API) for The Ticket System

### 2.3. ts-services ###

* business logic

---


## 3. GIT repository ##

* public

<https://bitbucket.org/tomo19/ticket-system>

Clone git repository:

```sh
git clone git@bitbucket.org:tomo19/ticket-system.git
```