package org.tomo.mmb.task.ts_services.core;

/**
 * <h1>Result of creating new ticket operation.</h1>
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public interface INewTicketResult {

    /**
     * <h1>Order in queue of the new created ticket.</h1>
     *
     * <b>Note:</b> After creating new ticket, this ticket is insert into queue
     * at last position. Order is number of this position.
     *
     * @return  int - position in queue (on the last place)
     */
    public int getQueueOrder();


    /**
     * <h1>New, created ticket.</h1>
     *
     * Entity of the new, created ticket.
     *
     * @return  {@link ITicket}
     * @see     ITicket
     */
    public ITicket getTicket();


}
