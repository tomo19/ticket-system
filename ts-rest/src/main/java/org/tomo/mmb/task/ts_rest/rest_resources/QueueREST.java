package org.tomo.mmb.task.ts_rest.rest_resources;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import org.tomo.mmb.task.ts_rest.MyConfigrationSingleton;
import org.tomo.mmb.task.ts_rest.messages.ErrorXMLEntity;
import org.tomo.mmb.task.ts_rest.messages.FirstActiveTicketXMLEntity;
import org.tomo.mmb.task.ts_rest.messages.ObjectFactory;
import org.tomo.mmb.task.ts_services.exception.MyBusinessException;

/**
 * <h1>Ticket System <b>queue</b> REST resource.</h1>
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
@Path("/queue")
public class QueueREST {

    private static final Logger logger = Logger.getLogger("QueueREST");

    @Context
    UriInfo context;

    public QueueREST() {
    }


    /**
     * <h1>GET method for getting first active ticket id.</h1>
     *
     * @return {@link javax.ws.rs.core.Response}
     */
    @GET
    @Path("/first-active-ticket")
    @Produces(MediaType.APPLICATION_XML)
    public Response getFirstActiveTicketId() {

        int first_active_ticket_id = -99;

        // get first active ticket from Ticket System
        // ---------------------------------------------------------------------
        //                        
        try {

            first_active_ticket_id = MyConfigrationSingleton.getInstance().getITicketSystem().getFirstActiveTicketNumber();

            logger.log(Level.INFO, "QueueREST.getFirstActiveTicketId(): first_active_ticket_id=" + first_active_ticket_id);
        } catch (MyBusinessException e) {

            // create Error REST Response with error message
            // -----------------------------------------------------------------
            //
            logger.log(Level.SEVERE, "QueueREST.getFirstActiveTicketId(): error during geeting first ticket from queue");
            logger.log(Level.SEVERE, "QueueREST.getFirstActiveTicketId(): Exception msg: ");
            logger.log(Level.SEVERE, e.getMessage());

            ErrorXMLEntity error_entity = new ErrorXMLEntity();

            error_entity.setId(e.getMyExceptionId());
            error_entity.setMessage(e.getMessage());

            JAXBElement<ErrorXMLEntity> error_entity_JAXB = new ObjectFactory().createError(error_entity);

            // RESPONSE ! ! !
            //
            return Response.status(Response.Status.BAD_REQUEST)
                    .encoding("UTF-8")
                    .entity(error_entity_JAXB)
                    .build();
        }

        // create POJO XML entity for conversion to XML (JAXB)
        // based on: org.tomo.mmb.tas.ts_rest.messages.xsd.TicketXMLEntity.xsd
        // ---------------------------------------------------------------------
        //
        FirstActiveTicketXMLEntity first_active_ticket_id_XML_entity = new FirstActiveTicketXMLEntity();

        first_active_ticket_id_XML_entity.setTicketId(first_active_ticket_id);

        JAXBElement<FirstActiveTicketXMLEntity> first_active_ticket_id_JAXB = null;

        first_active_ticket_id_JAXB = new ObjectFactory().createFirstActiveTicket(first_active_ticket_id_XML_entity);

        // create REST response
        // ---------------------------------------------------------------------
        //
        URI first_active_ticket_id_URI = UriBuilder.fromUri(this.context.getBaseUri())
                .path(TicketREST.class)
                .path(Integer.toString(first_active_ticket_id))
                .build();

        logger.log(Level.INFO, "QueueREST.getFirstActiveTicketId(): returned URL: " + first_active_ticket_id_URI.toString());

        // RESPONSE ! ! !
        //        
        return Response.ok()
                .location(first_active_ticket_id_URI)
                .encoding("UTF-8")
                .entity(first_active_ticket_id_JAXB)
                .build();
    }


    /**
     * <h1>GET method for destroying last active ticket from the ticket
     * queue.</h1>
     *
     * @return {@link javax.ws.rs.core.Response}
     */
    @DELETE
    @Path("/last-active-ticket")
    @Produces(MediaType.APPLICATION_XML)
    public Response destoryLastActivelTicket() {

        // destroy last active ticket from Ticket System
        // ---------------------------------------------------------------------
        //   
        try {
            MyConfigrationSingleton.getInstance().getITicketSystem().destroyLastActiveTicket();
        } catch (MyBusinessException e) {

            // create Error REST Response with error message
            // -----------------------------------------------------------------
            //
            logger.log(Level.SEVERE, "QueueREST.destroyLastActiveTicket(): error during geeting first ticket from queue");
            logger.log(Level.SEVERE, "QueueREST.destroyLastActiveTicket(): Exception msg: ");
            logger.log(Level.SEVERE, e.getMessage());

            ErrorXMLEntity error_entity = new ErrorXMLEntity();

            error_entity.setId(e.getMyExceptionId());
            error_entity.setMessage(e.getMessage());

            JAXBElement<ErrorXMLEntity> error_entity_JAXB = new ObjectFactory().createError(error_entity);

            // RESPONSE ! ! !
            //
            return Response.status(Response.Status.BAD_REQUEST)
                    .encoding("UTF-8")
                    .entity(error_entity_JAXB)
                    .build();
        }

        // RESPONSE ! ! !
        //        
        return Response.ok()
                .encoding("UTF-8")
                .entity("")
                .build();
    }


}
