package org.tomo.mmb.task.ts_services.core;

import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.tomo.mmb.task.ts_services.exception.MyBusinessException;

import static org.junit.Assert.assertEquals;
import org.tomo.mmb.task.ts_services.util.TicketIdSequence;

/**
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TicketSystemTest {

    private TicketSystem ticketSystem = null;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {

        this.ticketSystem = TicketSystem.getInstace();
    }


    @After
    public void after() {

        TicketSystem.test_removeInstanceReference();
        TicketIdSequence.test_removeInstanceReference();
    }


    @Test
    public void test1() {

        try {
            this.ticketSystem.getFirstActiveTicketNumber();
        } catch (MyBusinessException e) {
            assertEquals(4, e.getMyExceptionId());
        } catch (Exception e) {
            assertTrue(false);
        }

        try {
            this.ticketSystem.destroyLastActiveTicket();
        } catch (MyBusinessException e) {
            assertEquals(5, e.getMyExceptionId());
            return;
        } catch (Exception e) {
            assertTrue(false);
        }

        assertTrue(false);
    }


    @Test
    public void test2() {

        try {
            this.ticketSystem.createNewActiveTicket();

            this.ticketSystem.destroyLastActiveTicket();

            try {
                this.ticketSystem.getFirstActiveTicketNumber();
            } catch (MyBusinessException e) {
                assertEquals(4, e.getMyExceptionId());
                return;
            } catch (Exception e) {
                assertTrue(false);
            }
        } catch (Exception e) {
            assertTrue(false);
        }
    }


    @Test
    public void test3() {

        try {
            this.ticketSystem.createNewActiveTicket();

            this.ticketSystem.getFirstActiveTicketNumber();

            this.ticketSystem.destroyLastActiveTicket();

            try {
                this.ticketSystem.getFirstActiveTicketNumber();
            } catch (MyBusinessException e) {
                assertEquals(4, e.getMyExceptionId());
            } catch (Exception e) {
                assertTrue(false);
            }

            try {
                this.ticketSystem.destroyLastActiveTicket();
            } catch (MyBusinessException e) {
                assertEquals(5, e.getMyExceptionId());
                return;
            } catch (Exception e) {
                assertTrue(false);
            }

        } catch (Exception e) {
            assertTrue(false);
        }
    }


    @Test
    public void test4() {

        try {
            this.ticketSystem.createNewActiveTicket();

            this.ticketSystem.destroyLastActiveTicket();

            this.ticketSystem.createNewActiveTicket();

            this.ticketSystem.destroyLastActiveTicket();

            try {
                this.ticketSystem.getFirstActiveTicketNumber();
            } catch (MyBusinessException e) {
                assertEquals(4, e.getMyExceptionId());
            } catch (Exception e) {
                assertTrue(false);
            }

            try {
                this.ticketSystem.destroyLastActiveTicket();
            } catch (MyBusinessException e) {
                assertEquals(5, e.getMyExceptionId());
            } catch (Exception e) {
                assertTrue(false);
            }

            try {
                this.ticketSystem.getFirstActiveTicketNumber();
            } catch (MyBusinessException e) {
                assertEquals(4, e.getMyExceptionId());
            } catch (Exception e) {
                assertTrue(false);
            }

            try {
                this.ticketSystem.destroyLastActiveTicket();
            } catch (MyBusinessException e) {
                assertEquals(5, e.getMyExceptionId());
            } catch (Exception e) {
                assertTrue(false);
                return;
            }

        } catch (Exception e) {
            assertTrue(false);
        }

    }


    @Test
    public void test5() {

        try {
            List<ITicket> tickets = this.ticketSystem.getAllActiveTickets();

            assertEquals(0, tickets.size());

        } catch (Exception e) {
            assertTrue(false);
        }
    }


    @Test
    public void test6() {

        try {
            this.ticketSystem.createNewActiveTicket();

            List<ITicket> tickets = this.ticketSystem.getAllActiveTickets();

            assertEquals(1, tickets.size());
            assertEquals(1, tickets.get(0).getId());

        } catch (Exception e) {
            assertTrue(false);
        }
    }


}
