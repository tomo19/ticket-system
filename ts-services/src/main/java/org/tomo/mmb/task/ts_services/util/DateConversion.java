package org.tomo.mmb.task.ts_services.util;

import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <h1>Conversion from {@link java.util.Date} to
 * {@link javax.xml.datatype.XMLGregorianCalendar} for JAXB.</h1>
 *
 * <ul><li>Singleton</li></ul>
 * <p>
 * When you use JAXB generated Java classes from XSD schema, and you want to
 * have XSD {@code xs:date}, or {@code xs:time} element type in your XML
 * generated through these classes, JAXB generate Java beans, whose require
 * date/time in {@link javax.xml.datatype.XMLGregorianCalendar} object.
 * <p>
 * There is little complicated (and time consuming) conversion from
 * {@link java.util.Date} to {@link javax.xml.datatype.XMLGregorianCalendar}
 * object.
 *
 * {@link #convertDateForXML(java.util.Date) } will do this.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class DateConversion {

    /**
     * Singleton instance of this class.
     */
    private static DateConversion instance = null;

    // this is issue, we don't know, if DatatypeFactory is ThreadSafe
    //
//    private DatatypeFactory datatypeFactory = null;
//        
//    private DateConversion() throws DatatypeConfigurationException {
//        this.datatypeFactory = DatatypeFactory.newInstance();
//    }
    private DateConversion() {
    }


    /**
     * <h1>Return singleton instance of this class.</h1>
     *
     * First call will create a new singleton instance of this class.
     *
     * @return {@link DateConversion} - singleton instace
     */
    public static DateConversion getInstace() {

        if (DateConversion.instance == null) {

            synchronized (DateConversion.class) {
                DateConversion.instance = new DateConversion();
            }
        }

        return DateConversion.instance;
    }


    /**
     * <h1>Convert {@link java.util.Date} to
     * {@link javax.xml.datatype.XMLGregorianCalendar}.</h1>
     *
     * <b>Note:</b> this operation may be little time-consuming. Every call will
     * create new {@link javax.xml.datatype.DatatypeFactory} factory. It is
     * reusable instance, but there is some uncertanity about thread-safety of
     * this instance, because of this, we create new instance on every call of
     * this method.
     *
     * @param date date for conversion
     * @return {@link javax.xml.datatype.XMLGregorianCalendar} - date in
     * suitable Java type for JAXB processing
     * @throws DatatypeConfigurationException when there is some problem in
     * getting new instance of {@link javax.xml.datatype.DatatypeFactory},
     * needed for {@link java.util.Date} processing inner in this method.
     */
    public XMLGregorianCalendar convertDateForXML(final Date date) throws DatatypeConfigurationException {

        // little complicated conversion from Date object to somethink,
        // what we can pass to JAXB as date/time for XML generation,
        // generated JAXB Java Classes requires XMLGregorianCalendar
        //
        GregorianCalendar gregorian_calendar = new GregorianCalendar();

        gregorian_calendar.setTime(date);

        XMLGregorianCalendar XML_gregorian_calendar = null;

        // maybe time-consuming
        //
        XML_gregorian_calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorian_calendar);

        return XML_gregorian_calendar;
    }


}
