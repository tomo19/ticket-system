package org.tomo.mmb.task.ts_services.exception;

/**
 * <h1>My own exception - for business leve exception</h1>
 *
 * This is exactly same class as {@link MyException}, but only another name of
 * Java type (another Java class), for distinguishing between system and
 * business level exception.
 * <p>
 * For example, you can catch business exception separated from system
 * exception, because you know, that you can use description of business level
 * exception for error examplnation to user.
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class MyBusinessException extends MyException {

    public MyBusinessException(int exceptionId, String message) {
        super(exceptionId, message);
    }


}
