package org.tomo.mmb.task.ts_rest;

/**
 *
 * @author Tomas Kundis {@code <example.name@company.com>}
 */
public class ApiDescription {

    public static final String API_PREFIX = "/api/rest";

    /**
     * The Ticket System REST API Version.
     */
    public static final String VERSION = "v1.0.0";

    /**
     * Base path for this application. All resources URLs will be prefixed with
     * this base path.
     */
    public static final String ROOT_PATH_REST = API_PREFIX + "/" + VERSION;

}
